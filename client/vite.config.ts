import eslint from '@rollup/plugin-eslint';
import reactRefresh from '@vitejs/plugin-react-refresh';
import { UserConfig } from 'vite';
import { join, resolve } from 'path';
import tsconfigPaths from 'vite-tsconfig-paths';

const config: UserConfig = {
  plugins: [
    { ...eslint({ include: 'src/**/*.+(js|jsx|ts|tsx)' }), enforce: 'pre' },
    reactRefresh(),
    tsconfigPaths(),
  ],
  resolve: {
    alias: {
      '~': join(__dirname, 'src'),
      '~/public': join(__dirname, 'public'),
    },
  },
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:3001',
        // changeOrigin: true,
        // rewrite: (path) => path.replace(/^\/api/, '')
      },
    },
  },
};

export default config;
