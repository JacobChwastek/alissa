import React from 'react';
import { useWindowEvent } from './useWindowEvent';

export const useGlobalMouseUp = (callback: any) => useWindowEvent('mouseup', callback);

export const useGlobalMouseMove = (callback: any) => useWindowEvent('mousemove', callback);
