import { DICTIONARIES } from '@/constants';
import { CustomModal } from '../components/custom';

export const errorResolver = (error: any): IError => {
  if (error.response && error.response.data) {
    return {
      message: error.response.data.message,
      code: error.response.data.statusCode,
    };
  }
  return {
    message: DICTIONARIES.STATUS_CODES[500],
    code: 500,
  };
};

export const interceptGetError = (): void => {
  // console.log(error);
};

export const interceptPostError = (message?: string, code?: number): void => {
  CustomModal.displayCustomWarning({
    title: `Error ${code}`,
    content: `An error occurred during making request. Please try one more time.
        ${message ? `Error: ${message}` : ''}`,
  });
};
