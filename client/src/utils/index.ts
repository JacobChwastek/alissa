import { api } from './api';
import * as interceptors from './interceptors';

export {
  api,
  interceptors,
};
