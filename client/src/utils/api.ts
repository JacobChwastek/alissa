import axios, { AxiosError, AxiosResponse } from 'axios';

const api = axios.create({
  baseURL: '/api',
});

api.interceptors.response.use(
  (response: AxiosResponse) => response,
  (error: AxiosError) => Promise.reject(error),
);

export { api };
