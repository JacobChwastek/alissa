import { createGlobalStyle } from 'styled-components';
import FjallaOne from './FjallaOne-Regular.ttf';
import GudeaBold from './Gudea-Bold.ttf';
import GudeaItalic from './Gudea-Italic.ttf';
import Gudea from './Gudea-Regular.ttf';

const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: FjallaOne;
    src: url(${FjallaOne});
  }

  @font-face {
    font-family: Gudea-Bold;
    src: url(${GudeaBold});
  }

  @font-face {
    font-family: Gudea-Italic;
    src: url(${GudeaItalic});
  }

  @font-face {
    font-family: Gudea;
    src: url(${Gudea});
  }
`;

export default GlobalStyles;
