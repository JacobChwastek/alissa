import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { interceptors } from '@/utils';
import { AppThunk } from '../store';
import { RootState } from '../rootReducer';
import { DEFAULT_TYPES } from '../../constants';
import { auth } from '../../dal';

export const initialState: AuthState = {
  isAuth: false,
  error: { message: '', code: 200 },
  isLoading: false,
  user: DEFAULT_TYPES.DEFAULT_USER,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setLoading: (state, { payload }: PayloadAction<any>) => {
      state.isLoading = payload;
    },
    setUser: (state: AuthState, { payload }: PayloadAction<IUser>) => {
      state.user = payload;
      state.isAuth = true;
    },
    setLogOut: (state) => {
      state.isAuth = false;
      state.user = DEFAULT_TYPES.DEFAULT_USER;
    },
    setAuthFailed: (state, { payload }: PayloadAction<any>) => {
      state.error = payload;
      state.isAuth = false;
    },
  },
});

export const login = ({ password, email }: ILogin): AppThunk => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const currentUser = await auth.login({ password, email });
    dispatch(setUser(currentUser));
  } catch (error) {
    const resolvedError = interceptors.errorResolver(error);
    dispatch(setAuthFailed(resolvedError));
  } finally {
    dispatch(setLoading(false));
  }
};

export const logOut = (): AppThunk => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    await auth.logout();
    dispatch(setLogOut());
  } catch (error) {
    dispatch(setAuthFailed(error));
  } finally {
    dispatch(setLoading(false));
  }
};

export const {
  setUser, setLogOut, setLoading, setAuthFailed,
} = authSlice.actions;

export const authSelector = (state: RootState) => state.auth;

export default authSlice.reducer;
