import { AxiosError } from 'axios';
import { api } from '../utils/api';

const login = async ({ email, password }: ILogin) => api
  .post('/auth/login', { email, password })
  .then(res => res.data)
  .catch((err: AxiosError) => Promise.reject(err));

const logout = async () => api
  .post('/auth/logout')
  .catch((err: AxiosError) => Promise.reject(err));

const getMe = async () => api
  .post('/auth')
  .then(res => res.data)
  .catch((err: AxiosError) => Promise.reject(err));

export {
  login,
  logout,
  getMe,
};
