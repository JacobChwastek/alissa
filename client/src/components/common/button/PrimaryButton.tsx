import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../../../constants';

type Props = {
  text?: string;
  htmlType?: 'button' | 'submit' | 'reset';
}

const PrimaryButton = ({ text, htmlType }: Props) => (
  <Button
    type={htmlType || 'button'}
    className="primary-button"
  >
    {text || ''}
  </Button>
);

PrimaryButton.defaultProps = {
  text: '',
  htmlType: 'button',
};

const Button = styled.button`
  background-color: ${COLORS[1]};
  color: #FFFF;
  width: 360px;
  font-family: Gudea;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 30px;
  display: flex;
  align-items: center;
  text-align: center;
  text-transform: uppercase;
  height: 70px;
  outline: none;
  justify-content: center;
`;

export default PrimaryButton;
