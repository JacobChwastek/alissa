import React, { Component, createRef } from 'react';
import {
  Form,
  message,
} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { PasswordInput } from 'antd-password-input-strength';
import { AxiosError } from 'axios';
import { FormInstance } from 'antd/lib/form';
import { PrimaryButton } from '../button';
import { Input, Card } from '../index';
import { api, interceptors } from '../../../utils';

const { AuthCard } = Card;

type Props = {}

type State = {
  errorMessage: string,
  errorStatus: 'success' | 'error'
}

export default class RegisterForm extends Component<Props, State> {
  private formRef: React.RefObject<FormInstance<any>>;

  constructor(props: Props) {
    super(props);

    this.state = {
      errorMessage: '',
      errorStatus: 'success',
    };

    this.formRef = createRef<FormInstance>();
  }

  render() {
    const { errorMessage, errorStatus } = this.state;
    return (
      <AuthCard title="Register" bordered={false}>
        <Form
          ref={this.formRef}
          name="register"
          initialValues={{ remember: true }}
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
        >
          <Form.Item
            name="email"
            validateStatus={errorStatus}
            help={errorMessage}
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input
              onChange={this.onEmailChange}
              placeholder="Name"
              prefix={<UserOutlined />}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
            hasFeedback
          >
            <StyledInputPassword
              inputProps={{
                prefix: <LockOutlined />,
                placeholder: 'Password',
              }}
            />
          </Form.Item>
          <Form.Item
            name="confirm"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) return Promise.resolve();

                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
              }),
            ]}
          >
            <StyledInputPassword
              inputProps={{
                prefix: <LockOutlined />,
                placeholder: 'Confirm Password',
              }}
            />
          </Form.Item>
          <Form.Item className="form-item-button">
            <PrimaryButton text="Register" htmlType="submit" />
          </Form.Item>
        </Form>
      </AuthCard>
    );
  }

  onEmailChange = () => {
    const { errorMessage } = this.state;
    if (errorMessage) this.setState({ errorMessage: '', errorStatus: 'success' });
  }

  onFinish = (values: any) => {
    api
      .post('/auth/register', { ...values })
      .then(res => res.data)
      .then(() => {
        message.success('User successfully created');
        this.formRef.current?.resetFields();
      })
      .catch(this.handleErrorResponse);
  };

  onFinishFailed = () => {}

  handleErrorResponse = ({ response }: AxiosError) => {
    if (!response) return interceptors.interceptPostError();

    const { status, data } = response;

    if (status === 409) return this.setState({ errorStatus: 'error', errorMessage: data.message || '' });
    return interceptors.interceptPostError(data.message || '', status);
  }
}

const StyledInputPassword = styled(PasswordInput)`
        .ant-input {
      font-family: Gudea;
      font-style: normal;
      font-weight: normal;
      font-size: 18px !important;
      line-height: 24px;
    }
    .ant-input-prefix {
      font-size: 18px;
    }
    height: 60px;
`;
