import styled from 'styled-components';
import { Card } from 'antd';

const AuthCard = styled(Card)`
  width: 70%;
  background: rgba(255, 255, 255, 0.9);
  backdrop-filter: blur(4px);
  border-radius: 20px;
  display: flex;
  flex-direction: column;

  .ant-card-head {
    padding: 10% 0 10% 0;
  }

  .ant-card-head-title {
    font-family: Gudea;
    font-style: normal;
    font-weight: bold;
    font-size: 36px;
    line-height: 28px;
    display: flex;
    align-items: center;
    justify-content: center;
    letter-spacing: 0.75px;
    color: #000000;
  }
  .ant-card-body {
    flex: 1;
    display: flex;
    flex-direction: column;

    .ant-form {
      display: flex;
      flex-direction: column;
      flex: 1;

      .form-item-button {

        padding: 6% 0 10% 0;
        .ant-form-item-control-input-content{
          display: flex;
          justify-content: center;

          button {
            width: 60%;
            height: 50px;
          }
        }
      }
    }
  }
`;

export default AuthCard;
