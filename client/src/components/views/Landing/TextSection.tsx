import React from 'react';
import styled from 'styled-components';
import { PrimaryButton } from '../../common/button';

const Text = () => (
  <TextSection className="landing-text-section">
    <Header>Dicom management system</Header>
    <Title>Manage your dicom collection </Title>
    <Subtitle>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
      purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor
    </Subtitle>
    <PrimaryButton text="Register Now" />
  </TextSection>
);

const TextSection = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  flex-basis: 0;
  justify-content: end;
  max-height: 560px;
  height: 100%;
  justify-content: space-between;
    h1, h2, h3 {
    max-width: 70%;
  }
`;

const Header = styled.h3`
  font-family: Gudea;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 22px;
  display: flex;
  align-items: center;
  text-transform: uppercase;
  color: #FFFFFF;
  border-bottom: 2px solid #FFFFFF;
  width: max-content;
  padding-bottom: 5px;
`;

const Title = styled.h1`
font-family: Gudea;
font-style: normal;
font-weight: normal;
font-size: 66px;
line-height: 89px;
display: flex;
align-items: center;
text-transform: uppercase;
color: #FFFFFF;`;

const Subtitle = styled.h2`
font-family: Gudea;
font-style: normal;
font-weight: normal;
font-size: 18px;
line-height: 30px;
display: flex;
align-items: center;
text-transform: uppercase;
color: #FFFFFF;`;

export default Text;
