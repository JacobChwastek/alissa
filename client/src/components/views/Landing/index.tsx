import DoctorBlob from './DoctorBlob';
import TextSection from './TextSection';

export {
  DoctorBlob,
  TextSection,
};
