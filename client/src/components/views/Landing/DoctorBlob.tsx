import React from 'react';
import styled from 'styled-components';
import DoctorImg from '../../../assets/images/doctor-vector.svg';

const DoctorBlob = () => <Doctor src={DoctorImg} className="doctor-blob" />;

const Doctor = styled.img`
    flex-grow: 1;
    flex-basis: 0;
    height: 100%;
    max-height: 650px;
`;

export default DoctorBlob;
