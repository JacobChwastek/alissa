interface IUser {
  email: string
}

interface ILogin {
  email: string,
  password: string,
}

interface IError {
  message: string,
  code: number,
}

interface AuthError {
  message: string,
  code: number
}

interface AuthState {
  isAuth: boolean
  user: IUser
  isLoading: boolean
  error: AuthError
}
