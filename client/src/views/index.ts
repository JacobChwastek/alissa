import LandingView from './LandingView';
import LoginView from './LoginView';
import RegisterView from './RegisterView';

export {
  LandingView,
  LoginView,
  RegisterView,
};
