import React from 'react';
import { Row, Col } from 'antd';
import { useWindowSize } from '@/hooks';
import {
  sider, forms, container, Content,
} from '../components/common';

const { LoginForm } = forms;
const { AuthSider } = sider;
const { AuthContainer } = container;
// 992px
const LoginView: React.FC = () => (
  <AuthContainer>
    {
        window.innerWidth > 992
          ? (
            <Row gutter={[16, 16]} className="register-view-row--desktop">
              <Col span={6}>
                <AuthSider />
              </Col>
              <Col span={18}>
                <Content>
                  <LoginForm />
                </Content>
              </Col>
            </Row>
          )
          : (
            <>
              <Row gutter={[16, 16]} className="auth register-view-row--mobile">
                <AuthSider mobile />
              </Row>
              <Row gutter={[16, 16]} className="form register-view-row--mobile">
                <Content>
                  <LoginForm />
                </Content>
              </Row>
            </>
          )
      }

  </AuthContainer>
);

export default LoginView;
