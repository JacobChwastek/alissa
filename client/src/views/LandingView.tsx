import React from 'react';
import styled from 'styled-components';
import { views } from '../components';

const { Landing: { DoctorBlob, TextSection } } = views;

const LandingView = () => (
  <Container>
    <DoctorBlob />
    <TextSection />
  </Container>
);

const Container = styled.div`
  padding-top: 100px;
  height: 100vh;
  background-color: #7f5a83;
  background-image: linear-gradient(315deg, #7f5a83 0%, #0d324d 74%);
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export default LandingView;
