import React from 'react';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { Layout } from 'antd';
import { useShouldDisplay } from '@/hooks';
import { useSelector } from 'react-redux';
import { authSelector } from '@/store/slices/authSlice';
import Header from './components/common/layout/Header';
import GlobalFonts from './assets/fonts/ApplyGlobalFonts';

import {
  LandingView,
  LoginView,
  RegisterView,
} from './views';

const locations = [
  '/',
];

const App: React.FC = () => {
  const { shouldDisplay } = useShouldDisplay({ locations });
  const { user } = useSelector(authSelector);

  return (
    <Layout>
      <Header isVisible={shouldDisplay} />
      <GlobalFonts />
      <Switch>
        <Route path="/register" component={RegisterView} />
        <Route path="/login" component={LoginView} />
        <Route path="/" component={LandingView} />
      </Switch>
    </Layout>
  );
};

export default App;
