export class LoginUserDto {
  readonly email: string;
  readonly password: string;
  readonly id: string;
}
