interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

interface IAccountSettings {
  id: string;
  user: IUser;
}

interface IVolume {
  id: string;
  pathName: string;
  hostName: string;
  userName: string;
  password: string;
}

interface ISubscription {
  id: string;
  subscriptionId: string;
  createdAt: Date;
  user: IUser;
  currency: string;
  amount: number;
  periodStart?: Date;
  persiodEnd?: Date;
}
