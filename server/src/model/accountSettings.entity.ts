import {
  Entity,
  Column,
  OneToOne,
  JoinColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'account_settings' })
export class AccountSettings {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'registration_step' })
  registrationStep: number;

  @Column({ name: 'is_email_confirmed', default: false })
  isConfirmed: boolean;

  @OneToOne('User', 'accountSettings')
  @JoinColumn()
  user: IUser;
}
