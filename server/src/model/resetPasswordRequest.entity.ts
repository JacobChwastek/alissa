import {
  Entity,
  Column,
  OneToOne,
  PrimaryGeneratedColumn,
  JoinColumn,
} from 'typeorm';

@Entity({ name: 'reset_password_request' })
export class ResetPasswordRequest {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne('User', 'ResetPasswordRequest')
  @JoinColumn()
  user: IUser;

  @Column({ name: 'valid_until' })
  validUntil: Date;

  @Column({ name: 'is_used' })
  isUsed: boolean;
}
